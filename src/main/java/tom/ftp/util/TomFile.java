package tom.ftp.util;

import java.io.IOException;
import java.io.OutputStream;

public interface TomFile {
    void writeTo(OutputStream outputStream) throws IOException;

    String getRoot();

    String getPath();

    String getName();
}
