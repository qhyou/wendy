package tom.ftp.util;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.io.OutputStream;

public class TomFileImpl implements TomFile {
    private FTPClient client;
    private String root;
    private String path;
    private String name;

    public TomFileImpl(FTPClient client, String root, String path, String name) {
        this.client = client;
        this.root = root;
        this.path = path;
        this.name = name;
    }

    @Override
    public void writeTo(OutputStream outputStream) throws IOException {
        client.retrieveFile(TomFtpClient.touch(path, name), outputStream);
    }

    @Override
    public String getRoot() {
        return root;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "TomFileImpl{" +
                "root='" + root + '\'' +
                ", path='" + path + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
