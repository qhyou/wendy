package tom.ftp.util;

public class FtpConnInfo {
    private String user;
    private String pass;
    private String host;
    private int port;
    private String path;

    private String ftpAddr;

    public FtpConnInfo(String addr, String user, String pass, String host, int port, String path) {
        this.user = user;
        this.pass = pass;
        this.host = host;
        this.port = port;
        this.path = path;
        this.ftpAddr = addr;
    }

    public String getFtpAddr() {
        return ftpAddr;
    }

    public void setFtpAddr(String ftpAddr) {
        this.ftpAddr = ftpAddr;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "FtpConnInfo{" +
                "user='" + user + '\'' +
                ", pass='" + pass + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", path='" + path + '\'' +
                ", ftpAddr='" + ftpAddr + '\'' +
                '}';
    }
}
