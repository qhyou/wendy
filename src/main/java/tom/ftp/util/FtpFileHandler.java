package tom.ftp.util;

import java.io.IOException;

public interface FtpFileHandler {

    public void store(TomFile file) throws IOException;
}
