package tom.script.util;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BashCmd {
    protected String cmdScritpRoot;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public BashCmd(String cmdScritpRoot) {
        this.cmdScritpRoot = cmdScritpRoot;
    }

    protected CommandLine makeCmd(String shcmd){
        CommandLine cmdLine = new CommandLine("bash");
        cmdLine.addArgument(cmdScritpRoot + shcmd);
        return cmdLine;
    }

    protected void executeCmd(CommandLine cmdLine) throws Exception {
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        DefaultExecutor executor = new DefaultExecutor();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(60000);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);
//        resultHandler.waitFor();
    }

    protected void executeCmd(CommandLine cmdLine, int exitValue) throws Exception {
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(exitValue);
        ExecuteWatchdog watchdog = new ExecuteWatchdog(60000);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);
//        resultHandler.waitFor();
    }

    protected void executeCmd(CommandLine cmdLine, Long timeMills) throws Exception {
    	executeCmd(cmdLine, timeMills, false);
    }

    protected void executeCmd(CommandLine cmdLine, Long timeMills, boolean waitting) throws Exception {
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        DefaultExecutor executor = new DefaultExecutor();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(timeMills);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);
        if(waitting){
        	resultHandler.waitFor();
        }
    }

}
