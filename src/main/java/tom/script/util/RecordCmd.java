package tom.script.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;

public class RecordCmd extends BashCmd {
    public RecordCmd(String cmdScritpRoot) {
        super(cmdScritpRoot);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void record(String live_addr, File f, Long timeDuration){
        logger.info("record stream {} to file {}", live_addr, f);
        CommandLine cmdLine = makeCmd("/record.sh");
        Map map = new HashMap();
        map.put("file", f);
        cmdLine.addArgument(""+timeDuration);
        cmdLine.addArgument(live_addr);
        cmdLine.addArgument("${file}");
        cmdLine.setSubstitutionMap(map);
        try {
            executeCmd(cmdLine, timeDuration, true);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }

    }
}
