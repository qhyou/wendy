package tom.script.util;

import org.apache.commons.exec.CommandLine;

import java.io.File;

public class PlayCmd extends BashCmd {
    public PlayCmd(String cmdScritpRoot) {
        super(cmdScritpRoot);
    }

    public void play(File f, String rtmpServerPath){

    }

    public void live(Long time, String live_addr, String rtmpServerPath){
        logger.info("live stream {} to rtmp proxy {}", live_addr, rtmpServerPath);
        CommandLine cmdLine = makeCmd("/live.sh");
        cmdLine.addArgument(live_addr);
        cmdLine.addArgument(rtmpServerPath);
        cmdLine.addArgument(""+time);
        try {
            executeCmd(cmdLine,time);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }


    }
}
