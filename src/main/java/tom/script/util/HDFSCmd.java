package tom.script.util;

import org.apache.commons.exec.CommandLine;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class HDFSCmd extends BashCmd {

    public HDFSCmd(String cmdScritpRoot) {
        super(cmdScritpRoot);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void upload(File f, String path) {
        logger.info("publish file {} to hdfs {}", f, path);
        CommandLine cmdLine = makeCmd("/hdfs/upload.sh");
		Map map = new HashMap();
        map.put("file", f);
        cmdLine.addArgument("${file}");
        cmdLine.addArgument(path);
        cmdLine.addArgument(f.getName());
        cmdLine.setSubstitutionMap(map);
        try {
            executeCmd(cmdLine);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void downaload(String path, String name, File f) {
        logger.info("download to file {} from hdfs {}", f, path);
        CommandLine cmdLine = makeCmd("/hdfs/download.sh");
        cmdLine.addArgument(path);
        cmdLine.addArgument(name);
        Map map = new HashMap();
        map.put("file", f);
        cmdLine.addArgument("${file}");
        cmdLine.setSubstitutionMap(map);
        try {
            executeCmd(cmdLine);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }
    }

}
