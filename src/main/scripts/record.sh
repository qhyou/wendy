#!/bin/sh -e
TIME_SLICE=$1
LIVE_ADDR=$2
LOCAL_FILE=$3
echo "record remote live stream to local file. $LIVE_ADDR, $LOCAL_FILE, $TIME_SLICE is Seconds"
echo "/home/genvision/apps/ffmpeg/ffmpeg -t $TIME_SLICE -i $LIVE_ADDR -c copy $LOCAL_FILE"
/home/genvision/apps/ffmpeg/ffmpeg -t $TIME_SLICE -i $LIVE_ADDR -c copy $LOCAL_FILE