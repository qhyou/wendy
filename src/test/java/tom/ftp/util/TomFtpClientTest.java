package tom.ftp.util;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

@Ignore
public class TomFtpClientTest {

    @Test
    public void downloadWithAuth() throws IOException {
        String ftpAddr = "ftp://127.0.0.1:2121/abc";
        TomFtpClient tfc = TomFtpClient.connect(ftpAddr).withAuth("anonymous", "a@b.cn");
        tfc.downloadAll("", new FtpFileHandler() {

            @Override
            public void store(TomFile file) {
                try {
                    File root = new File("/Users/xuhaixiang/var/12");
                    File f = new File(root, file.getName());
                    FileOutputStream fos = new FileOutputStream(f);
                    file.writeTo(fos);
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void downloadWithAnonymous() throws IOException {
        String ftpAddr = "ftp://127.0.0.1:2121/abc";
        TomFtpClient.connect(ftpAddr).downloadAll("", new FtpFileHandler() {
            @Override
            public void store(TomFile file) {
                try {
                    File root = new File("/Users/xuhaixiang/var/12");
                    File f = new File(root, file.getName());
                    FileOutputStream fos = new FileOutputStream(f);
                    file.writeTo(fos);
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Test(expected = IOException.class)
    public void downloadWithConnException() throws IOException {
        String ftpAddr = "ftp://127.0.0.1:1111/abc";
        TomFtpClient.connect(ftpAddr).downloadAll("", new FtpFileHandler() {
            @Override
            public void store(TomFile file) {

            }
        });
    }

    @Test
    public void downloadOne() throws IOException {
        String ftpAddr = "ftp://127.0.0.1:2121/abc";
        TomFtpClient.connect(ftpAddr).download("/java/alice/ep", new FtpFileHandler() {
            @Override
            public void store(TomFile file) throws IOException {
                System.out.println(file);
            }
        });
    }

    @Test
    public void downloadFile() throws IOException {
        String ftpAddr = "ftp://127.0.0.1:2121/abc";
        TomFtpClient.connect(ftpAddr).downloadFile("/java/alice/ep/AliceApplication.java", new FtpFileHandler() {
            @Override
            public void store(TomFile file) throws IOException {
                System.out.println(file);
            }
        });

    }

}
